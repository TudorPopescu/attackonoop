package RPG.Entities;

public class EnemyFactory {

    public Enemy make(int level){

        Enemy randEnemy = new Enemy(level);
        return randEnemy;
    }
}